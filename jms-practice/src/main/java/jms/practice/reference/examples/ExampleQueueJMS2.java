package jms.practice.reference.examples;

import jms.practice.service.JndiService;

import javax.jms.*;

public class ExampleQueueJMS2 {

    public static void produce(String msg) {
        JndiService jndiService = new JndiService();
        ConnectionFactory connectionFactory = jndiService.getDefaultConnectionFactory();

        try(JMSContext context = connectionFactory.createContext()) {
            TextMessage message = context.createTextMessage(msg);
            context.createProducer().send(jndiService.getQueue(), message);
        }
    }

    public static void consume() {
        JndiService jndiService = new JndiService();
        ConnectionFactory connectionFactory = jndiService.getDefaultConnectionFactory();

        try(JMSContext context = connectionFactory.createContext()) {
                JMSConsumer consumer = context.createConsumer(jndiService.getQueue());
                String body = consumer.receiveBody(String.class);
                System.out.println(body);
            }
    }

}
