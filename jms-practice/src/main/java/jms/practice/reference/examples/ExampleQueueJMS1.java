package jms.practice.reference.examples;


import jms.practice.service.JndiService;

import javax.jms.*;

public class ExampleQueueJMS1 {

    public static void produce() {
        ConnectionFactory connectionFactory;
        Connection connection = null;
        try{
            JndiService jndiService = new JndiService();
            connectionFactory = jndiService.getDefaultConnectionFactory();
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(jndiService.getQueue());
            TextMessage textMessage = session.createTextMessage("Hello, I am a message");
            messageProducer.send(textMessage);
            System.out.println("Message sent");
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void consume() {
        ConnectionFactory connectionFactory;
        Connection connection = null;

        try{
            JndiService jndiService = new JndiService();
            connectionFactory = jndiService.getDefaultConnectionFactory();
            connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
            MessageConsumer messageConsumer = session.createConsumer(jndiService.getQueue());
            TextMessage textMessage = (TextMessage) messageConsumer.receive();
            String body = textMessage.getText();
            System.out.println(body);
        } catch (JMSException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
