package jms.practice.service;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JndiService {

    private InitialContext initialContext;
    private ConnectionFactory defaultConnectionFactory;
    private ConnectionFactory durableTopicConnectionFactory;
    private ConnectionFactory durableTopicConnectionFactory2;
    private ConnectionFactory durableTopicConnectionFactory3;
    private Queue queue;
    private Topic topic;

    public JndiService() {
        try {
            initialContext = new InitialContext();
            queue = (Queue) initialContext.lookup("jms/P2PQueue");
            topic = (Topic) initialContext.lookup("jms/PSTopic");
            defaultConnectionFactory = (ConnectionFactory)
                    initialContext.lookup("jms/__defaultConnectionFactory");
            durableTopicConnectionFactory = (TopicConnectionFactory)
                    initialContext.lookup("jms/__durableTopicConnectionFactory");
            durableTopicConnectionFactory2 = (TopicConnectionFactory)
                    initialContext.lookup("jms/__durableTopicConnectionFactory2");
            durableTopicConnectionFactory3 = (TopicConnectionFactory)
                    initialContext.lookup("jms/__durableTopicConnectionFactory3");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public ConnectionFactory getDefaultConnectionFactory() {
        return defaultConnectionFactory;
    }

    public ConnectionFactory getDurableTopicConnectionFactory() { return durableTopicConnectionFactory;}

    public ConnectionFactory getDurableTopicConnectionFactory2() { return durableTopicConnectionFactory2;}

    public ConnectionFactory getDurableTopicConnectionFactory3() { return durableTopicConnectionFactory3;}

    public InitialContext getInitialContext() {
        return initialContext;
    }

    public Queue getQueue() {
        return queue;
    }

    public Topic getTopic() {
        return topic;
    }
}
