package jms.practice.receiver;

import jms.practice.service.JndiService;

import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.TextMessage;

public class TopicReceiverForSharedSubscribers extends TopicReceiver {

    private String sharedSubscriptionName;

    public TopicReceiverForSharedSubscribers(String receiverName, JndiService jndiService, String sharedSubscriptionName){
        super(receiverName, jndiService);
        this.sharedSubscriptionName = sharedSubscriptionName;
    }

    @Override
    public void consume() {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createSharedConsumer(jndiService.getTopic(), sharedSubscriptionName);
                TextMessage message = (TextMessage) consumer.receive();
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void consumeAndReply(String replyMessage) {

    }
}
