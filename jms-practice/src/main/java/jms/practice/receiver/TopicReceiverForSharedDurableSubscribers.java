package jms.practice.receiver;

import jms.practice.service.JndiService;

import javax.jms.*;

public class TopicReceiverForSharedDurableSubscribers extends TopicReceiver {

    private String sharedSubscriptionName;

    public TopicReceiverForSharedDurableSubscribers(String receiverName, JndiService jndiService, String sharedSubscriptionName){
        super(receiverName, jndiService);
        this.sharedSubscriptionName = sharedSubscriptionName;
    }

    @Override
    public void consume() {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createSharedDurableConsumer(jndiService.getTopic(), sharedSubscriptionName);
                TextMessage message = (TextMessage) consumer.receive();
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void consumeAndReply(String replyToMessage) {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createSharedDurableConsumer(jndiService.getTopic(), "sharedSubscriptionName");
                TextMessage message = (TextMessage) consumer.receive();
                if (!replyToMessage.isEmpty()) {
                    replyTo(message.getJMSReplyTo(), "reply message");
                }
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }
    }

    public void replyTo(Destination destination, String replyMessage) {
        try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            context.createProducer().send(destination, replyMessage);
        }
    }

}
