package jms.practice.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import jms.practice.testobject.SendableObject;
import jms.practice.service.JndiService;

import javax.jms.*;
import java.util.ArrayList;
import java.util.List;

public class QueueReceiver implements Receiver {

    private JndiService jndiService;
    private String receiverName;

    public QueueReceiver(String receiverName, JndiService jndiService){
        this.receiverName = receiverName;
        this.jndiService = jndiService;
    }


    //Calling this method will make sure the receiver maintains consuming messages whilst
    //the application is running
    @Override
    public void consume() {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createConsumer(jndiService.getQueue());
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, consumer.receiveBody(String.class)));
            }
        }
    }

    @Override
    public void consume(int sessionMode) {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext(sessionMode)) {
                JMSConsumer consumer = context.createConsumer(jndiService.getQueue());
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, consumer.receiveBody(String.class)));
                if (sessionMode == Session.CLIENT_ACKNOWLEDGE) {
                    context.acknowledge();
                    System.out.println("Acknowledged door client");
                }
            }
        }
    }

    @Override
    public void consumeAndReply(String replyMessage) {
        //still to be implemented
    }

    @Override
    public void consumeTransacted(boolean isCommit, int messagesInTransaction) {
        String message;
        int count = 0;
        try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext(Session.SESSION_TRANSACTED)) {
            JMSConsumer consumer = context.createConsumer(jndiService.getQueue());

            while(true){
                message = consumer.receiveBody(String.class);
                System.out.println(String.format("Just called receive on consumer for message: [%s]",
                        message));
                count++;
                if (count == messagesInTransaction && isCommit) {
                    System.out.println("Now commiting messages on consumer side");
                    context.commit();
                    count = 0;
                } else if (count == messagesInTransaction && !isCommit) {
                    System.out.println("Now rolling back messages on consumer side");
                    context.rollback();
                    count = 0;
                }

            }
        }
    }

    public void listen(QueueMessageListener listener, int sessionMode) {
        try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext(sessionMode)) {
            JMSConsumer consumer = context.createConsumer(jndiService.getQueue());
            consumer.setMessageListener(listener);

            //need to keep this thread alive and make sure JMSContext does not close. Otherwise, the
            //message listener will not work.
            while(true){}
        }
    }


    //Example code for receiving a single TextMessage
    public String getTextMessage() {
        try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            JMSConsumer consumer = context.createConsumer(jndiService.getQueue());
            //print the message for testing purposes
            String body = consumer.receiveBody(String.class);
            System.out.println(body);
            //return the message
            return body;
        }
    }

    public <T> Object getObjectFromJson(Class<T> aClass) {
        Object object = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            object = mapper.readValue(getTextMessage(), aClass);
            System.out.println(String.format("Object consumed: [%s], class: [%s]",
                    object.toString(), object.getClass()));
        } catch(Exception e) {
            e.printStackTrace();
        } return object;

    }

    //Example code for receiving a single ObjectMessage
    public Message getObjectMessage() {
        try  (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            JMSConsumer consumer = context.createConsumer(jndiService.getQueue());
            //print the object for testing purposes

            String obj = consumer.receiveBody(SendableObject.class).toString();
            System.out.println(obj);
            //return the object
            return consumer.receive();
        }
    }


}
