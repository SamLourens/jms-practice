package jms.practice.receiver;

import jms.practice.service.JndiService;

import javax.jms.*;

public class TopicReceiver implements Receiver {

    JndiService jndiService;
    String receiverName;

    public TopicReceiver(String receiverName, JndiService jndiService){
        this.receiverName = receiverName;
        this.jndiService = jndiService;
    }


    @Override
    public void consume() {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createConsumer(jndiService.getTopic());
                TextMessage message = (TextMessage) consumer.receive();
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void consume(int sessionMode) {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createConsumer(jndiService.getTopic());
                TextMessage message = (TextMessage) consumer.receive();
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void consumeAndReply(String replyToMessage) {
        while(true) {
            try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
                JMSConsumer consumer = context.createConsumer(jndiService.getTopic());
                TextMessage message = (TextMessage) consumer.receive();
                // From the message we can extract the replyto headerfield, which was set by the producer.
                // This field is of type "Destination", either a Queue, TemporaryQueue, Topic or
                // TemporaryTopic
                replyTo(message.getJMSReplyTo(), replyToMessage);
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void consumeTransacted(boolean isCommit, int messagesInTransaction) {
        // Still to be implemented
    }


    public void replyTo(Destination destination, String replyMessage) {
        try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            context.createProducer().send(destination, replyMessage);
        }
    }

}


