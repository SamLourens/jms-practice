package jms.practice.receiver;

import javax.jms.*;

public class QueueMessageListener implements MessageListener {

    String listenerName;

    public QueueMessageListener(String listenerName) {
        this.listenerName = listenerName;
    }

    @Override
    public void onMessage(Message message) {
        try {
            System.out.println(String.format("Listener [%s] received message: [%s]",
                    this.listenerName, message.getBody(String.class)));
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
