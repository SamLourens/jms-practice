package jms.practice.receiver;

public interface Receiver {

    void consume();

    void consume(int sessionMode);

    void consumeAndReply(String replyMessage);

    void consumeTransacted(boolean isCommit, int messagesInTransaction);
}
