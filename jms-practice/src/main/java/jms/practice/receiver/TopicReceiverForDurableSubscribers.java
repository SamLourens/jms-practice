package jms.practice.receiver;

import jms.practice.service.JndiService;

import javax.jms.*;

public class TopicReceiverForDurableSubscribers extends TopicReceiver {

    private String subscription;
    private ConnectionFactory connectionFactory;

    public TopicReceiverForDurableSubscribers(String receiverName, JndiService jndiService, ConnectionFactory connectionFactory, String subscription){
        super(receiverName, jndiService);
        this.subscription = subscription;
        this.connectionFactory = connectionFactory;
    }

    @Override
    public void consume() {
        while(true) {
            try (JMSContext context = connectionFactory.createContext()) {
                JMSConsumer consumer = context.createDurableConsumer(jndiService.getTopic(), subscription);
                TextMessage message = (TextMessage) consumer.receive();
                System.out.println(String.format("Receiver [%s] consumed message: [%s]",
                        this.receiverName, message.getText()));
            } catch(JMSException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void consumeAndReply(String replyMessage) {

    }

}
