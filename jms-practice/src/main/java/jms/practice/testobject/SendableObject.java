package jms.practice.testobject;

import java.io.Serializable;

//In order for an instance of this type to be sendable through an ObjectMessage, it needs to be Serializable.
public class SendableObject implements Serializable {

    private String objName;
    private String objAge;
    
    //Converting JSON to object with the ObjectMapper.readValue() method requires a constructor that takes no parameters
    public SendableObject(){
        //Empty constructor
    }

    public SendableObject(String objName, String objAge) {
        this.objName = objName;
        this.objAge = objAge;
    }

    public String getObjAge() {
        return objAge;
    }

    public String getObjName() {
        return objName;
    }

    @Override
    public String toString(){
        return String.format("Name: %s, Age: %s", this.objName, this.objAge);
    }
}
