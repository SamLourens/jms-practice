package jms.practice.sender;

import java.util.ArrayList;
import java.util.List;

public interface Sender {

    void sendTextMessage(String text);

    void sendTextMessage(String text, int deliveryMode, long timeToLive);

    //It is possible to get a reply from the receiver by setting up a temporary queue or topic
    // where the receiver can send a message back to.
    void sendTextMessageWithReplyTo(String text);

    void processTransaction(List<String> messages, boolean isCommit);

    default void sendMultipleTextMessages(int offset) {
        String message;
        while (true) {
            message = "Message " + offset++;
            try {
                Thread.sleep(3000);
                sendTextMessage(message);
            }
            catch (RuntimeException e) {
                System.out.println(String.format("Caught RuntimeException: %s", e.getMessage()));
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    default void sendMultipleTextMessagesTransacted(int messagesInSingleTransaction, int offset, boolean isCommit) {
        List<String> messages = new ArrayList<>();
        String message;
        int currentTransactionMessageCount = 0;
        while (true) {
            messages.add("Message " + offset++);
            currentTransactionMessageCount++;
            if (messagesInSingleTransaction == currentTransactionMessageCount) {
                processTransaction(messages, isCommit);
                messages = new ArrayList<>();
                currentTransactionMessageCount = 0;
            }
        }
    }

    default void sendMultipleTextMessages(int offset, int deliveryMode, long timeTolive) {
        String message;
        while (true) {
            message = "Message " + offset++;
            try {
                Thread.sleep(3000);
                sendTextMessage(message, deliveryMode, timeTolive);
            }
            catch (RuntimeException e) {
                System.out.println(String.format("Caught RuntimeException: %s", e.getMessage()));
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
