package jms.practice.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jms.practice.service.JndiService;

import javax.jms.*;
import java.io.Serializable;
import java.util.List;

public class QueueSender implements Sender {

    private JndiService jndiService;
    private String senderName;

    public QueueSender(String senderName, JndiService jndiService){
        this.senderName = senderName;
        this.jndiService = jndiService;
    }

    @Override
    public void sendTextMessage(String text) {
        try(JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            TextMessage textMessage = context.createTextMessage(text);
            context.createProducer().send(jndiService.getQueue(), textMessage);
            System.out.println(String.format("Sender [%s] produced message: [%s]",
                    this.senderName, text));
        }
    }

    @Override
    public void sendTextMessage(String message, int deliveryMode, long timeToLive) {
        try(JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            TextMessage textMessage = context.createTextMessage(message);
            context.createProducer().setDeliveryMode(deliveryMode).setTimeToLive(timeToLive)
                    .send(jndiService.getQueue(), textMessage);
            System.out.println(String.format("Sender [%s] produced message: [%s]",
                    this.senderName, message));
        }
    }

    @Override
    public void sendTextMessageWithReplyTo(String text) {
        /// Still to be implemented
    }

    @Override
    public void processTransaction(List<String> messages, boolean isCommit) {
        try(JMSContext context = jndiService.getDefaultConnectionFactory().createContext(Session.SESSION_TRANSACTED)) {
            JMSProducer producer = context.createProducer();
            for (String message: messages) {
                context.createTextMessage(message);
                System.out.println(String.format("Now calling send on producer for message: [%s]", message));
                producer.send(jndiService.getQueue(), message);
            }
            try{
                Thread.sleep(10000);
                if (isCommit) {
                    System.out.println("Now commiting messages");
                    context.commit();
                } else {
                    System.out.println("Now rolling back messages");
                    context.rollback();
                }
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    // When sending an ObjectMessage, the object must implement Serializable
    public void sendObjectMessage(Serializable obj) {
        try(JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            ObjectMessage objectMessage = context.createObjectMessage(obj);
            context.createProducer().send(jndiService.getQueue(), objectMessage);
        }
    }

    public void sendObjectAsJson(Object object, int deliveryMode, long timeToLive ) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(object);
            System.out.println(String.format("Object converted to JSON: %s", json));
            sendTextMessage(json, deliveryMode, timeToLive);
        } catch (JsonProcessingException e) {
            System.out.println("Mapping object to JSON failed...");
        }
    }

}
