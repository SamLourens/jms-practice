package jms.practice.sender;

import jms.practice.service.JndiService;

import javax.jms.*;
import java.util.List;

public class TopicSender implements Sender {

    JndiService jndiService;
    String senderName;

    public TopicSender(String senderName, JndiService jndiService){
        this.senderName = senderName;
        this.jndiService = jndiService;
    }


    @Override
    public void sendTextMessage(String message) {
        try(JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            TextMessage textMessage = context.createTextMessage(message);
            context.createProducer().send(jndiService.getTopic(), textMessage);
            System.out.println(String.format("Sender [%s] produced message: [%s]",
                    this.senderName, message));
        }
    }

    @Override
    public void sendTextMessage(String message, int deliveryMode, long timeToLive) {
        try(JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            TextMessage textMessage = context.createTextMessage(message);
            context.createProducer().setDeliveryMode(deliveryMode).setTimeToLive(timeToLive)
                    .send(jndiService.getTopic(), textMessage);
            System.out.println(String.format("Sender [%s] produced message: [%s]",
                    this.senderName, message));
        }
    }

    @Override
    public void sendTextMessageWithReplyTo(String message){
        try (JMSContext context = jndiService.getDefaultConnectionFactory().createContext()) {
            TextMessage textMessage = context.createTextMessage(message);
            // To this TemporaryQueue the receiver should send a reply
            Queue tempQueue = context.createTemporaryQueue();
            JMSProducer producer = context.createProducer();
            producer.setJMSReplyTo(tempQueue);
            producer.send(jndiService.getTopic(), textMessage);
            System.out.println(String.format("Sender [%s] produced message: [%s]",
                    this.senderName, message));
            TextMessage replyMessage = (TextMessage) context.createConsumer(tempQueue).receive();
            System.out.println(String.format("Sender %s received reply message: [%s]",
                    this.senderName, replyMessage.getText()));
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processTransaction(List<String> messages, boolean isCommit) {
        // still to be implemented
    }


}
