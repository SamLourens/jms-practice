package jms.practice;
import jms.practice.receiver.QueueMessageListener;
import jms.practice.receiver.QueueReceiver;
import jms.practice.sender.QueueSender;
import jms.practice.service.JndiService;

import javax.jms.DeliveryMode;
import javax.jms.Session;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main {

    private static JndiService jndiService = new JndiService();

    public static void main(String[] args) {
        //
    }


    public static void start(Runnable methodToRun){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(methodToRun);
    }
}

